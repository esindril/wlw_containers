db.scrape_status.insert(
		{ url: "http://35.156.119.192",
		  scraper: "wlw.scraper.lhc_iframe_scraper.LHCScraper",
		  online_status: 0,
		  scraper_status: 0,
		  ranking: 0,
		  modifiedTS: new Timestamp(),
		  config : {
			  chat: {
				  username: "WLW Scrape User",
				  initial_msg: "Are you online?",
				  response_msg: "Thank you for your support. WLW has now ranked your site as online!"
			  },
			  scraper: {
				  loops: 10,
				  sleep_per_loop: 10
			  }
		  }
		});

db.scrape_status.insert(
		{ url: "http://35.158.30.228/",
		  scraper: "wlw.scraper.zopim_scraper.ZopimScraper",
		  online_status: 0,
		  scraper_status: 0,
		  ranking: 0,
		  modifiedTS: new Timestamp(),
		  config : {
			  chat: {
				  username: "WLW Scrape User",
				  initial_msg: "Are you online?",
				  response_msg: "Thank you for your support. WLW has now ranked your site as online!"
			  },
			  scraper: {
				  loops: 10,
				  sleep_per_loop: 10
			  }
		  }
		});
