use admin
db.createUser(
  {
    user: "dbAdmin",
    pwd: "abc123",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
  }
)
db.auth("dbAdmin", "abc123");
use wlw
db.createUser(
  {
    user: "wlw",
    pwd: "scrape",
    roles: [
       { role: "readWrite", db: "wlw" }
    ]
  }
)
