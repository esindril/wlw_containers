#!/usr/bin/env bash

echo "Spawning application cluster ..."
docker-compose up -d

# Mongodb configuration, wait for the server to start
while !(docker exec mongodb /bin/sh -c 'mongo --eval "print(\"waited for connection\")"'); do
    echo "Waiting for mongodb ..."
    sleep 1
done

echo "Creating mongodb users ..."
docker cp db.createUsers.js mongodb:/tmp/db.createUsers.js
docker exec mongodb /bin/sh -c "mongo localhost:27017 < /tmp/db.createUsers.js"
echo "Adding scrape test data ..."
docker cp db.scrape.test.js mongodb:/tmp/db.scrape.test.js
docker exec mongodb /bin/sh -c "mongo localhost:27017 < /tmp/db.scrape.test.js"
